## **O**：

- Today we learned about three major design patterns: Command Pattern, Observer Pattern, and Strategy Pattern.
- What impressed me deeply is that the main differences between Command Pattern and Strategy Pattern are: different focus points, different character functions, and different usage scenarios.
- Not all development scenarios are suitable for using design patterns. Design patterns are a tool that can bring convenience to us in complex development processes, and we should not use them just to use them. Learning design patterns requires a long period of sedimentation and accumulation. When we can naturally use design patterns in development, we can count it as learning.
- In the afternoon, I mainly learned about the knowledge system of Code refactoring, learned how to find Code smell in my own code and others' code, found many magic codes that I hadn't found for many years, and learned how to improve it



## **R：**

- Today's learning task is relatively easy, but given a new teamwork, all members of our group need time to learn about CI/CD


## **I：**

- Nice


## **D：**

- Cultivate good development habits and avoid writing code with Code Smell.
