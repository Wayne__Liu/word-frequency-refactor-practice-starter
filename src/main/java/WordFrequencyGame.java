import java.util.*;

import java.util.stream.Collectors;

public class WordFrequencyGame {
    private final String WORD_SPLIT_REGEX = "\\s+";

    private final String CALCULATE_ERROR = "Calculate Error";

    public String countWordsFrequency(String inputStr) {
        try {
            return countWordsFrequencyWithOutExceptionHandling(inputStr);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private String countWordsFrequencyWithOutExceptionHandling(String inputStr) {
        String[] splitInput = inputStr.split(WORD_SPLIT_REGEX);
        List<Word> wordList = convertToObject(splitInput);
        Map<String, List<Word>> wordMap = getListMap(wordList);
        wordList = sortByFrequency(wordMap);
        return splicingWordsAndFrequency(wordList);
    }

    private Map<String, List<Word>> getListMap(List<Word> wordList) {
        return wordList.stream()
                .collect(Collectors.groupingBy(Word::getValue));
    }

    private List<Word> convertToObject(String[] input) {
        return Arrays.stream(input)
                .map(s -> new Word(s, 1))
                .collect(Collectors.toList());
    }

    private List<Word> sortByFrequency(Map<String, List<Word>> wordMap) {
        return wordMap.entrySet().stream()
                .map(entry -> new Word(entry.getKey(), entry.getValue().size()))
                .sorted(Comparator.comparingInt(Word::getWordCount).reversed())
                .collect(Collectors.toList());
    }

    private String splicingWordsAndFrequency(List<Word> wordList) {
        return wordList.stream()
                .map(word -> word.getValue() + " " + word.getWordCount())
                .collect(Collectors.joining("\n"));
    }
}
